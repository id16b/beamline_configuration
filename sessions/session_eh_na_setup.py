
from bliss.setup_globals import *

load_script("session_eh_na.py")

print("")
print("Welcome to your new 'session_eh_na' BLISS session !!")
print("")
print("You can now customize your 'session_eh_na' session by changing files:")
print("   * /session_eh_na_setup.py ")
print("   * /session_eh_na.yml ")
print("   * /scripts/session_eh_na.py ")
print("")