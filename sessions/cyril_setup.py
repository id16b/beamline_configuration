import gevent 
import atexit


from bliss.setup_globals import *

load_script("cyril.py")

user_script_homedir("/users/blissadm/local/id16b.git/id16b/")


###############################################################################
###############################################################################
###############################################################################

from bliss.common.logtools import elogbook
elogbook.disable()

# SCAN_SAVING.base_path = "/data/id16b/inhouse1/cyril/databliss/"


from bliss import current_session
current_session.disable_esrf_data_policy()


###############################################################################
###############################################################################
###############################################################################
