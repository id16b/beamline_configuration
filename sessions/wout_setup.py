
from bliss.setup_globals import *

load_script("wout.py")

print("")
print("Welcome to your new 'wout' BLISS session !! ")
print("")
print("You can now customize your 'wout' session by changing files:")
print("   * /wout_setup.py ")
print("   * /wout.yml ")
print("   * /scripts/wout.py ")
print("")