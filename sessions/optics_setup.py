
from bliss.setup_globals import *

load_script("optics.py")

print("")
print("Welcome to your new 'optics' BLISS session !! ")
print("")
print("You can now customize your 'optics' session by changing files:")
print("   * /optics_setup.py ")
print("   * /optics.yml ")
print("   * /scripts/optics.py ")
print("")