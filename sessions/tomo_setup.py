#
# Bliss session "tomo" setup file.
#

from bliss.setup_globals import *

#### TOMO Objects
# from bliss.setup_globals import tomo_config, full_tomo, ref_displ

# Tomo presets
from tomo.presets import DefaultChainImageCorrOnOffPreset, ImageCorrOnOffPreset
from tomo.presets import ReferenceMotorPreset
from tomo.presets import FastShutterPreset, DarkShutterPreset, DetectorSafetyPreset


#### TOMO Configuration utils
# menu to configure tomo  ->  setup(full_tomo)
from tomo.utils import setup

# Shortcuts for users: TOMO_N ENERGY DARK_N REF_N  EXP_TIME  etc.
from tomo.standard import *

# from id16a.controllers.shutter import set_ximea_shutter_mode, set_fs_manual, set_fs_external

load_script("tomo.py")
# load_script("id16b_custom.py")

from id16b.tomo_utils import *



#### MDT
# If true, don't use the shutters for the scans.
MDT = False
if MDT:
    print("")
    print_html("<danger>========================================================</danger>")
    print_html("<danger>=                                                      =</danger>")
    print_html("<danger>=    Be careful MDT=True; shutters will not be used    =</danger>")
    print_html("<danger>=                                                      =</danger>")
    print_html("<danger>========================================================</danger>")
    print("")


# Choose a default config (can have more than one but not here)
tomo_config.set_active()


#### machinfo
try:
    from bliss.setup_globals import machinfo
    machinfo.initialize()
except:
    print("\nNo machine information available!\n")


# Force opiom MUX   ??? to be done in presets ?
opmux.switch('ACQ_TRIG', 'COUNT')



#### PRESETS

# ???
# a preset that can be added (via tomo_config.add_scan_preset).
# Use imagecorr.on or off to apply auto FF correction on a ct
#imagecorrpreset = ImageCorrOnOffPreset()


# Declare OPIOM multiplexer presets defined in scripts/tomo.py
step_preset = MultiplexerPresetStep()
continuous_preset = MultiplexerPresetContinuous()

# Preset to move ref motors out before a flat.
ref_motor_preset = ReferenceMotorPreset(ref_displ)

if MDT:
    frontend_preset = None
    fastshutter_preset = None
    darkshutter_preset = None
    detsafety_preset = None
else:
    frontend_preset = None  #FrontEndPreset(machinfo, fe)
    fastshutter_preset = FastShutterPreset(fshut)
    darkshutter_preset = DarkShutterPreset(bsh3, open_shutter=True) # shutter is re-opened after dark
    detsafety_preset = None  #DetectorSafetyPreset(shut2)


#### Load standard chain description into DEFAULT_CHAIN
#### DEFAULT_CHAIN is used by Bliss standard scans (ascan...)
from bliss.setup_globals import DEFAULT_CHAIN
tomo_single_chain = config.get("tomo_single_chain")
DEFAULT_CHAIN.set_settings(tomo_single_chain["chain_config"])

#### Add Chain presets
# https://bliss.gitlab-pages.esrf.fr/bliss/master/scan_engine_preset.html#chainpreset
# DEFAULT_CHAIN.add_preset()
# step_preset


#### default mux -> step
set_acq_mode('step')


#### SCANS

# Assign *fastshutter_preset* to scans who require it.
#if fastshutter_preset is not None:
#    tomo_config.add_scan_preset(['flat', 'count', 'return_ref', 'tiling', 'CONTINUOUS', 'STEP'], fastshutter_preset)

# Assign *darkshutter_preset* to scans who require it.
if darkshutter_preset is not None:
    tomo_config.add_scan_preset(['dark'], darkshutter_preset)

#### TOMO SCANS presets.
tomo_config.add_scan_preset(['flat', 'dark', 'count', 'return_ref', 'tiling', 'STEP'], step_preset)
tomo_config.add_scan_preset(['CONTINUOUS'], continuous_preset)
tomo_config.add_scan_preset(['flat'], ref_motor_preset)


#### FullTomo Sequence preset
# fulltomo_sequence_preset = FullTomoSequencePreset('fulltomo_sequence_preset')   # logs ???
# full_tomo.add_sequence_preset(fulltomo_sequence_preset, 'fulltomo_sequence_preset')



# to access to common fscans in session
ftimescan = fscan_tomo.get_runner('ftimescan')

fscan = fscan_tomo.get_runner('fscan')
fscan.pars.motor = srot


# flint()

setup_print("------------  tomo session setup finished  ---------------")


