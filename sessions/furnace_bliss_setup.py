import gevent 
import atexit

from bliss.setup_globals import *

# load_script("furnace_bliss.py")


###############################################################################
###############################################################################
###############################################################################

from bliss.common.logtools import elogbook
elogbook.disable()


from bliss import current_session
current_session.disable_esrf_data_policy()

ush = "/users/opid16b/bliss_user/"
user_script_homedir(ush)
print(f"Use {ush} as user script homedir")


###############################################################################

l1 = nano_loop
l1.plot() 

print("loop Tango Device:", l1.device.name())

print("regulation controllet ip: ", nanodac_pool._config.get('controller_ip'))


monitoring = timescan(1, l1, run=False)
# Disable the progress bar
monitoring._scan_progress = None
# Finally start the scan in background
spawmedTimescan = gevent.spawn(monitoring.run)


atexit.register(spawmedTimescan.kill)  


print ("""

NANODAC DU POOL sampenv-nanodac04.esrf.fr

""")

###############################################################################
###############################################################################
###############################################################################
