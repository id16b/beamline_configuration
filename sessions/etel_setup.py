
from bliss.setup_globals import *

load_script("etel.py")

print("")
print("Welcome to your new 'etel' BLISS session !! ")
print("")
print("You can now customize your 'etel' session by changing files:")
print("   * /etel_setup.py ")
print("   * /etel.yml ")
print("   * /scripts/etel.py ")
print("")