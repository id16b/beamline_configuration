from bliss.setup_globals import *
from bliss.shell.standard import print_html

# print("loading fshutter_setup.py")

user_script_homedir("/users/blissadm/local/id16b.git/id16b/")

load_script("fshutter.py")


# https://www.soundsnap.com/tags/engine_acceleration


ID16B = "<strong>  <aaa fg='ansiwhite' bg='#e800ac'> ID16B </aaa></strong>"
print()
print()
print_html(f"  {ID16B} fshutter BLISS session: IcePap fast shutter configuration.")  # ???? attention print scrambling :(
print_html("            type<strong> fshut</strong> for info ")
print()
print(fshut.__info__())

