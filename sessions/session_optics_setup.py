
from bliss.setup_globals import *

load_script("session_optics.py")

print("")
print("Welcome to your new 'session_optics' BLISS session !!")
print("")
print("You can now customize your 'session_optics' session by changing files:")
print("   * /session_optics_setup.py ")
print("   * /session_optics.yml ")
print("   * /scripts/session_optics.py ")
print("")