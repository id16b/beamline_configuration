import bliss.common.plot as plot_module

import tango
from tango import DevFailed



# flint()

# ct(0.1, zyla)


def er():

    # Lima/lrs urls and proxies
    lima_url = "id16b/limaccds/zyla"
    lrs_url = "id16b/roi2spectrum/zyla"
    lima_proxy = tango.DeviceProxy(lima_url)
    lrs_proxy = tango.DeviceProxy(lrs_url)

    fflint = plot_module.get_flint()
    plot_proxy = fflint.get_live_plot(image_detector="zyla")
    plot_proxy.focus()
    selections = []
    selections = plot_proxy.select_shapes( selections, kinds=[ "lima-vertical-profile", "lima-horizontal-profile", ], )
    for roi in selections:
        print(roi.name, roi.x, roi.y, roi.width, roi.height)

    lrs_proxy.start()
    lrs_proxy.clearAllRois()

    roi_id_list = lrs_proxy.addnames([roi.name])

    # lrs_proxy.setRois([roi_id_list[0], roi.x, roi.y, roi.width, roi.height])
    lrs_proxy.setRois([roi_id_list[0], roi.x, roi.y, roi.width, roi.height])

    print("GETNAMES :",  lrs_proxy.getnames())


    lrs_proxy.setRoiModes([roi.name, "LINES_SUM"])
    lrs_proxy.getRoiModes([roi.name])



# selet roi
# rename it  # or not ?
# apply




# -> selections  [<208,301> <476 x 174> <horizontal>]

