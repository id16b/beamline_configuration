
# tomo.py scripts for Bliss 'tomo' session
#

# print(" ------------------ loading scripts/tomo.py ---------------------")

from bliss.setup_globals import DEFAULT_CHAIN
from bliss.config.static import get_config
from bliss.scanning.scan import ScanPreset

from tomo.sequencepreset import SequencePreset

load_script("id16b_custom.py")

opmux = get_config().get("opmux")


def set_acq_mode(mode):
    """
    Helper for multiplexer config.
    Set in continuous or step mode:
    * opiom rack multiplexer
    * default chain
    """
    mode = mode.lower()
    if mode == 'step':
        # set_custom_chain('p201') # ???
        print("[opmux] switched to COUNT")
        opmux.switch('ACQ_TRIG', 'COUNT')
        opmux.switch('SHUTTER_SOFT', 'CLOSED')
        opmux.switch('SHUTTER', 'SPEC')
    elif mode == 'continuous':
        # set_custom_chain() # ???
        print("[opmux] switched to ZAP_GATE")
        opmux.switch('ACQ_TRIG', 'ZAP_GATE')
        opmux.switch('SHUTTER', 'SOFT')
        opmux.switch('SHUTTER_SOFT', 'CLOSED')
    else:
        raise ValueError(f"unknown mode {mode}, should be in ['step', 'continuous']")


class MultiplexerPresetStep(ScanPreset):
    def __init__(self):
        super().__init__()

    def prepare(self, scan):
        set_acq_mode('step')

    def start(self, scan):
        pass

    def stop(self, scan):
        pass

class MultiplexerPresetContinuous(ScanPreset):
    def __init__(self):
        super().__init__()

    def prepare(self, scan):
        set_acq_mode('continuous')

    def start(self, scan):
        opmux.switch('SHUTTER_SOFT', 'OPEN')

    def stop(self, scan):
        set_acq_mode('step')


class FullTomoSequencePreset(SequencePreset):
    def __init__(self, name, config=None):
        if config is None:
            config = {}
        super().__init__(name, config)

    def prepare(self):
        """
        This methods is called during tomo acquisition prepare.
        """
        print(f"{self.name} is preparing")
        pass

    def start(self):
        """
        This methods is called before running tomo acquisition.
        """
        print(f"{self.name} is starting")
        pass

    def stop(self):
        """
        This methods is called at the end of tomo acquisition.
        """
        print(f"{self.name} is stoping")
        pass

    def cleanup(self):
        """
        This methods is always called at the end of tomo acquisition.
        """
        pass

print("scripts/tomo.py loaded")

