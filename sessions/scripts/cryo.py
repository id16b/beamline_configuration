

import gevent
import time
import numpy as np


ls336_loop = config.get("ls336_loop")

"""
to load this script :
    load_script("cryo.py")
"""


spawmedTican = None

ls336_loop.deadband = 0.005


# try:
#     if spawmedTimescan is not None:
#         print("a scan is running ??? (spawmedTimescan is not None)")
# except NameError:
#     print("new spawnedTimescan")
#     spawmedTimescan = None
# 

def cryo_timescan_start():
    global spawmedTimescan
    plotselect("ls336_in:ls336_in", "ls336_out:ls336_out")
    ls336_loop.plot()
    print("------ start time scan ---")
    time.sleep(5)
    spawmedTimescan = gevent.spawn(timescan, 5.0, ls336_loop.counters)
    print("------- time scan started -----")


def cryo_timescan_stop():
    global spawmedTimescan
    print("------ stop timescan ------")
    if spawmedTimescan is not None:
        spawmedTimescan.kill()
    else:
        print("spawmedTimescan is none ???")
    print("-----------timescan stopped --------")
    


def cryo_regulate_on():
    """
    Regulate the heat power by adjusting the He flow arriving to the
    cryo. When a power lower than 30% is reached in the Lakeshore,
    the heva valve is opened by 1 step (about 0.036deg).
    """
    nb_values = 100

    # Steps depends on used dewar
    heva_step = 1  # U2
#    heva_step = 5  # U3
#  2022/12/9 U1 does not work as expected

    power_threshold = 30
    
    # intialize an array with all values with a first read.
    values = np.zeros(nb_values)
    last_read = float(ls336_out.value_percent)
    values += last_read

    _idx = 0       # global index 
    _idx_arr = 0   # array index

    sleep_time = 0.1
    
    while(True):
        if _idx_arr > (nb_values -1):
            _idx_arr = 0
            sleep_time = 5.0

        last_read = float(ls336_out.value_percent)
        values[_idx_arr] = last_read
        average_power = np.median(values)  # better use median than average to limit effects of extrem values.
        print(f"POWER {_idx}: {last_read}  -  {average_power} ")
        time.sleep(sleep_time)

        _idx_arr += 1
        _idx += 1
        
        # Check the threshold.
        if (average_power < power_threshold) and (_idx > nb_values):
            print(f"OH oh oh I'm under {power_threshold}")
            print(f"I will move heva from {heva.position} by {heva_step} steps  in 5 seconds")
            time.sleep(5)
            umvr(heva, heva_step)
            print(f"new heva position: {heva.position}")

            print("wait 5s...", flush=True)
            time.sleep(5)

            # After having open the valve, re-calculate quickly an average.
            _idx_arr = 0
            values = np.zeros(nb_values)
            last_read = float(ls336_out.value_percent)
            values += last_read
            for ii in range(nb_values):
                time.sleep(0.1)
                last_read = float(ls336_out.value_percent)
                values[_idx_arr] = last_read
                _idx_arr += 1

                average_power = np.median(values)
                print("\r", end="", flush=True)
                print(f"average power = {average_power}", end="", flush=True)

            print("")


